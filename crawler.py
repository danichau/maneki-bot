#!/usr/bin/env python3
""" fetch ohlcv data from exchanges and save to database """
import argparse
import asyncio
import sys
import time

import apsw
import ccxt.async as ccxt
import pandas as pd

from utils import TF2SECOND

HOLD_SECOND = 16
OHLCV_CONNEC = apsw.Connection('ohlcv.db')
OHLCV_CONNEC.setbusytimeout(4000)
OHLCV_CURSOR = OHLCV_CONNEC.cursor()


def save_ohlcv(table_name: str, ohlcvs) -> None:
    for _ in ohlcvs:
        _[0] /= 1000
        if len(_) == 7:
            assert table_name[:4] == 'okex'
            _ = _[:6]
        OHLCV_CURSOR.execute('insert or ignore into '+table_name+' values'+str(tuple(_)))


async def fetch_ohlcv(ex, symbol, timeframe, start, end):
    table_name = '{}_{}_{}'.format(ex.id, symbol.replace('/', '').replace('.', ''), timeframe)
    OHLCV_CURSOR.execute('create table if not exists %s (timestamp int primary key, open real, high real, low real, close real, volume real)' % table_name)
    cursor = start
    print('fetching {}'.format(table_name))
    while cursor < end:
        try:
            OHLCV_CURSOR.execute('select 1 from {} where timestamp={};'.format(table_name, cursor))
            if OHLCV_CURSOR.fetchall() == []:
                ohlcv = await ex.fetch_ohlcv(symbol, timeframe, cursor*1000)
                if len(ohlcv) < 2:
                    break
                ohlcv = sorted(ohlcv, key=lambda i: i[0])
                ohlcv = ohlcv[:-1]
                save_ohlcv(table_name, ohlcv)
                cursor = int(ohlcv[-1][0])+TF2SECOND[timeframe]
            else:
                OHLCV_CURSOR.execute('select min(timestamp) from {0} t0 where {1}<=t0.timestamp and not exists(select 1 from {0} t1 where t1.timestamp-t0.timestamp={2});'.format(table_name, cursor, TF2SECOND[timeframe]))
                cursor = OHLCV_CURSOR.fetchall()[0][0]+TF2SECOND[timeframe]
        except (ccxt.DDoSProtection, ccxt.RequestTimeout) as _:
            sys.stderr.write('{}, retrying in {}s...\n'.format(_.args, HOLD_SECOND))
            await asyncio.sleep(HOLD_SECOND)
    return pd.read_sql_query('select * from {} where timestamp>={} and timestamp<{};'.format(table_name, start, end), OHLCV_CONNEC)


async def fetch_ohlcv_(ex, pair, timeframe, start, end):
    ex = getattr(ccxt, ex)(dict(enableRateLimit=True))
    res = await fetch_ohlcv(ex, pair, timeframe, start, end)
    await ex.close()
    return res


loop = asyncio.get_event_loop()
def fetch_ohlcv_sync(ex, pair, timeframe, start, end):
    asyncio.set_event_loop(loop)
    res = loop.run_until_complete(fetch_ohlcv_(ex, pair, timeframe, start, end))
    return res


async def fetch_pairs(ex, pairs):
    while True:
        try:
            await ex.load_markets()
            break
        except (ccxt.AuthenticationError, ccxt.ExchangeNotAvailable) as _:
            sys.stderr.write('{}, skipping...\n'.format(_.args))
            return
        except (ccxt.RequestTimeout, ccxt.DDoSProtection) as _:
            sys.stderr.write('{}, retrying in {}s...\n'.format(_.args, HOLD_SECOND))
            await asyncio.sleep(HOLD_SECOND)
    for pair in pairs:
        if pair not in ex.markets.keys():
            continue
        for _tf in ['1m', '1h', '1d']:
            end = (int(time.time())//TF2SECOND[_tf]-1)*TF2SECOND[_tf]
            start = end-512*TF2SECOND[_tf]
            assert start > 0
            await fetch_ohlcv(ex, pair, _tf, start, end)


async def fetch_pairs_from_exchanges(exchas, pairs):
    tasks = []
    for ex in exchas:
        tasks.append(fetch_pairs(getattr(ccxt, ex)(dict(enableRateLimit=True)), pairs))
    await asyncio.gather(*tasks)


async def main():
    args = argparse.ArgumentParser()
    args.add_argument(
        '-e', '--exchanges',
        help='list of exchanges',
        default='binance,bitfinex')
    args.add_argument(
        '-p', '--pairs',
        help='list of trading pairs',
        default='BTC/USD,ETH/BTC,EOS/BTC,XMR/BTC')
    args = args.parse_args()
    await fetch_pairs_from_exchanges(
        args.exchanges.split(','),
        args.pairs.split(','))


if __name__ == '__main__':
    try:
        asyncio.get_event_loop().run_until_complete(main())
    except KeyboardInterrupt:
        sys.stderr.write('keyboard interrupt, exiting...\n')
        sys.exit()
