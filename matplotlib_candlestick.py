#!/usr/bin/env python3
""" plot cadlestick chart with matplotlib """
import argparse
import time

import matplotlib
matplotlib.use('agg')
from matplotlib import pyplot as plt
from matplotlib.dates import date2num, DateFormatter
from matplotlib.ticker import LinearLocator, ScalarFormatter
from mpl_finance import candlestick_ohlc
from talib import MACD, RSI
import pandas as pd

from crawler import fetch_ohlcv_sync
from utils import TF2SECOND

BLACK = '#303030'
GREEN = '#3e50b4'
RED = '#ff3f80'


def plot_candlestick(axis, ohlcv):
    candlestick_ohlc(axis, ohlcv.values, colorup=GREEN, colordown=RED, width=0.0001)
    axis.grid(color='w', alpha=.5)
    axis.tick_params(axis='y', colors='w')
    axis.spines['bottom'].set_color('w')
    axis.spines['bottom'].set_alpha(.5)
    axis.spines['top'].set_color('w')
    axis.spines['top'].set_alpha(.5)
    axis.spines['left'].set_color('w')
    axis.spines['left'].set_alpha(.5)
    axis.spines['right'].set_color('w')
    axis.spines['right'].set_alpha(.5)
    axis.set_yscale('log')
    axis.yaxis.set_major_locator(LinearLocator())
    axis.yaxis.set_major_formatter(ScalarFormatter())


def plot_volume(axis, ohlcv):
    axis.fill_between(ohlcv['timestamp'].values, ohlcv['volume'].values, alpha=.5)
    axis.axes.yaxis.set_ticklabels([])
    axis.set_ylabel('price & volume')
    axis.yaxis.label.set_color('w')
    axis.set_ylim(0, 2.618*ohlcv['volume'].max())
    axis.spines['bottom'].set_color('w')
    axis.spines['bottom'].set_alpha(.5)
    axis.spines['top'].set_color('w')
    axis.spines['top'].set_alpha(.5)
    axis.spines['left'].set_color('w')
    axis.spines['left'].set_alpha(.5)
    axis.spines['right'].set_color('w')
    axis.spines['right'].set_alpha(.5)


def plot_rsi(axis, ohlcv):
    axis.set_ylabel('RSI(14)')
    axis.yaxis.label.set_color('w')
    axis.tick_params(axis='y', colors='w')
    axis.spines['bottom'].set_color('w')
    axis.spines['bottom'].set_alpha(.5)
    axis.spines['top'].set_color('w')
    axis.spines['top'].set_alpha(.7)
    axis.spines['left'].set_color('w')
    axis.spines['left'].set_alpha(.7)
    axis.spines['right'].set_color('w')
    axis.spines['right'].set_alpha(.7)
    axis.set_yticks([30, 70])
    axis.yaxis.tick_right()
    axis.axhline(70, color=RED, linewidth=.5)
    axis.axhline(30, color=GREEN, linewidth=.5)
    _x = ohlcv['timestamp'].values[14:]
    _y = RSI(ohlcv['close'].values, timeperiod=14)[14:]
    axis.plot(_x, _y)
    axis.fill_between(_x, _y, 70, where=(_y > 70), facecolor=RED, edgecolor=RED, alpha=0.5)
    axis.fill_between(_x, _y, 30, where=(_y < 30), facecolor=GREEN, edgecolor=GREEN, alpha=0.5)


def plot_macd(axis, ohlcv):
    axis.set_ylabel('MACD(12,26,9)')
    axis.tick_params(axis='x', colors='w')
    axis.tick_params(axis='y', colors='w')
    axis.yaxis.label.set_color('w')
    axis.spines['bottom'].set_color('w')
    axis.spines['bottom'].set_alpha(.7)
    axis.spines['top'].set_color('w')
    axis.spines['top'].set_alpha(.5)
    axis.spines['left'].set_color('w')
    axis.spines['left'].set_alpha(.7)
    axis.spines['right'].set_color('w')
    axis.spines['right'].set_alpha(.7)
    axis.yaxis.tick_right()
    axis.xaxis.set_major_formatter(DateFormatter('%Y-%m-%d\n%H:%M'))
    _x = ohlcv['timestamp'].values
    macd, sign, hist = MACD(ohlcv['close'].values, fastperiod=12, slowperiod=26, signalperiod=9)
    axis.plot(_x, macd, color=GREEN, linewidth=.5)
    axis.plot(_x, sign, color=RED, linewidth=.5)
    axis.fill_between(_x, 0, hist, alpha=.5)


def plot(ex, pair, timeframe, filename):
    end = (int(time.time())//TF2SECOND[timeframe]-1)*TF2SECOND[timeframe]
    start = end-256*TF2SECOND[timeframe]
    ohlcv = fetch_ohlcv_sync(ex, pair, timeframe, start, end)
    ohlcv['timestamp'] = pd.to_datetime(ohlcv['timestamp'], unit='s')
    ohlcv['timestamp'] = ohlcv['timestamp'].apply(date2num)
    fig = plt.figure(figsize=(12.8, 7.2), facecolor=BLACK)
    fig.text(.382, .5, pair+', '+timeframe+', '+ex, fontsize=32, color='w', alpha=0.5, ha='center')
    ax_v = plt.subplot2grid((5, 1), (1, 0), rowspan=3, facecolor=BLACK)
    ax_c = ax_v.twinx()
    ax_r = plt.subplot2grid((5, 1), (0, 0), sharex=ax_c, facecolor=BLACK)
    ax_m = plt.subplot2grid((5, 1), (4, 0), sharex=ax_c, facecolor=BLACK)
    plot_rsi(ax_r, ohlcv)
    plot_candlestick(ax_c, ohlcv)
    plot_volume(ax_v, ohlcv)
    plot_macd(ax_m, ohlcv)
    plt.setp(ax_r.get_xticklabels(), visible=False)
    plt.setp(ax_c.get_xticklabels(), visible=False)
    fig.tight_layout()
    plt.subplots_adjust(hspace=0)
    plt.savefig(filename, format='jpeg', facecolor=fig.get_facecolor())
    plt.close(fig)


def main():
    args = argparse.ArgumentParser()
    args.add_argument('-e', '--exchange', default='bitfinex')
    args.add_argument('-p', '--pair', default='BTC/USD')
    args.add_argument('-t', '--timeframe', default='1h')
    args = args.parse_args()
    plot(args.exchange, args.pair, args.timeframe, 'plot.jpeg')


if __name__ == '__main__':
    main()
