#!/usr/bin/env python3
""" telegram bot """
from io import BytesIO
import logging

from matplotlib_candlestick import plot
from telegram import ReplyKeyboardMarkup, ReplyKeyboardRemove
from telegram.ext import CommandHandler, Updater
from telegram.ext.dispatcher import run_async
import ccxt

CCXT = {i:getattr(ccxt, i)(dict(enableRateLimit=True)) for i in ['bitfinex', 'binance']}
LOGGER = logging.getLogger(__name__)


def error(bot, update, error):
    """Log Errors caused by Updates."""
    LOGGER.warning('Update "%s" caused error "%s"', update, error)


def candlestick(bot, update, args):
    ex = None
    pair = None
    tf = None
    for a in args:
        if a in CCXT:
            ex = CCXT[a]
            break
    if ex is None:
        update.message.reply_text('exchange unrecognized')
        return
    for a in args:
        if a in ex.markets:
            pair = a
            break
    if pair is None:
        update.message.reply_text('pair unrecognized')
        return
    for a in args:
        if a in ex.timeframes:
            tf = a
            break
    if tf is None:
        update.message.reply_text('timeframe unrecognized')
        return
    buff = BytesIO()
    plot(ex.id, pair, tf, buff)
    buff.seek(0)
    update.message.reply_photo(photo=buff, quote=False)
    buff.close()


def btc_1m(bot, update):
    candlestick(bot, update, ['BTC/USD', '1m', 'bitfinex'])


def eth_1m(bot, update):
    candlestick(bot, update, ['ETH/USD', '1m', 'bitfinex'])


def btc_1h(bot, update):
    candlestick(bot, update, ['BTC/USD', '1h', 'bitfinex'])


def eth_1h(bot, update):
    candlestick(bot, update, ['ETH/USD', '1h', 'bitfinex'])


def btc_1d(bot, update):
    candlestick(bot, update, ['BTC/USD', '1d', 'bitfinex'])


def eth_1d(bot, update):
    candlestick(bot, update, ['ETH/USD', '1d', 'bitfinex'])


def main():
    for c in CCXT.values():
        c.load_markets()
    updater = Updater("513788604:AAGhW45frSXN4VU4WDjo6lIHTbwaZ4KZ3DU")
    dp = updater.dispatcher
    dp.add_handler(CommandHandler('candlestick', candlestick, pass_args=True))
    dp.add_handler(CommandHandler('btc_1m', btc_1m))
    dp.add_handler(CommandHandler('eth_1m', eth_1m))
    dp.add_handler(CommandHandler('btc_1h', btc_1h))
    dp.add_handler(CommandHandler('eth_1h', eth_1h))
    dp.add_handler(CommandHandler('btc_1d', btc_1d))
    dp.add_handler(CommandHandler('eth_1d', eth_1d))
    dp.add_error_handler(error)
    updater.start_polling()
    updater.idle()


if __name__ == '__main__':
    logging.basicConfig(
        datefmt='%b %d %H:%M:%S',
        format='%(asctime)s maneki %(name)s: %(levelname)s %(message)s',
        level=logging.INFO)
    main()
